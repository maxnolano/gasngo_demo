<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Api\v1\Info\InfoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api/v1', 'prefix' => '/v1'], function(){
	Route::get('kazpetrol-orders', [InfoController::class, 'kazpetrolOrders']);
    Route::get('kazpetrol-vehicles', [InfoController::class, 'kazpetrolVehicles']);
});