<?php

namespace App\Http\Controllers\Api\v1\Info;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use App\Services\v1\Info\InfoService;

class InfoController extends ApiController
{
    private Request $request;

    public function kazpetrolOrders(Request $request){
        $result = (new InfoService())->kazpetrolOrders($request);
        return $result;
    }

    public function kazpetrolVehicles(Request $request){
        $result = (new InfoService())->kazpetrolVehicles($request);
        return $result;
    }
}