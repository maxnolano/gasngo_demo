<?php

namespace App\Services\v1\Info;

use App\Services\v1\BaseService;
use Illuminate\Http\Request;
use GuzzleHttp\{Client, Exception\GuzzleException, Psr7\Request as ClientRequest};
use Mockery\Undefined;
use Telegram\Bot\Laravel\Facades\Telegram;
use Illuminate\Support\Facades\DB;

/**
 * Service for Info
 */
class InfoService extends BaseService
{
    // GuzzleHttp client example
    protected Client $client;

    /**
     * Class constructor
     * @param Request $request
     */
    public function __construct()
    {

        $this->client = new Client();
    }

    public function kazpetrolOrders($orderId, $type = null): Array
    {
        try {

            $orders = DB::connection('server_mysql')
                ->table('crm_gasngo_orders as gord')
                ->select(
                    'gord.id',
                    'gord.transaction_id',
                    'gord.machinery_id',
                    'gord.demand_quantity',
                    'gord.deliver_quantity',
                    'gord.status',
                    'gord.item_code',
                    'gord.item_name',
                    'gord.created_at',
                    'gord.updated_at'
                )
                ->get()
                ->toArray();

            return $orders;
            
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }

    public function kazpetrolVehicles($orderId, $type = null): Array
    {
        try {

            $machineries = DB::connection('server_mysql')
                ->table('crm_machineries as mch')
                ->select(
                    'mch.id',
                    'mch.model',
                    'mch.brand',
                    'mch.name',
                    'mch.plate_num',
                    'mch.addedby_user_id',
                    'mch.created_at',
                    'mch.updated_at',
                    'mch.gas_type',
                    'mch.monthly_limit',
                    'mch.daily_limit',
                    'mch.balance',
                    'mch.daily_expences',
                    'mch.monthly_expences',
                    'mch.item_code',
                    'mch.item_name'
                )
                ->get()
                ->toArray();

            return $machineries;
            
        }catch(\Exception $e) {
            return $this->errService($e->getMessage());
        }
    }
}